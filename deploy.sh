# Print commands to the screen
set -x

# Catch errors
set -euo pipefail

echo "Deploying application to production"